# Code standards 


## coding

1. Meaningful variables, classes and method names.
2. CamelCase for Class and camelCase for methods and variables
3. Use of comment to explain code
4. Use of Javadoc comments to document classes.
5. Exception handling to handle runtime errors
6. Clean up resources after use.
7. Make code modular by breaking into smaller methods and classes.
8. use access modifiers such as public, private and protected to control access to varibales and methods.
9. Use inheritance, polymorphism and interface to promote code reuse.
10. Avoiding code duplication by using helper methods or classes.

## Package structure

1. Root package is `com.dgv`. All other packages can be named accordingly example a package that handles customer can be written as `com.dgv.customer` or `com.dgv.utils.email`.
2. The test packages for a package say `com.dgv.customer` will be written as `com.dgv.customer.test`

### **Database Desing in DGV**

1. Master tables must contain following field memebers
   1. CREATED_ON
   2. CREATED_BY
   3. MODIFIED_ON
   4. MODIFIED_BY
   5. DELETE_FLAG
2. TRANSACTION TABLE
   1. CREATED_AT
   2. MODIFIED_AT
   3. DELETE_FLAG


## Spring boot package structure

1. Controller packages:

    - Contains Restful Web services.
    - Controllers annotated with @RestController
    - package name for say books resource will be `com.dgv.books.controller`

2. Service package:

    - Contains business logic of application
    - Responsible for interacting with databases and external API's to retrieve and manipulate data. 
    - package name can go for an entity say book will go as `com.dgv.book.service`

3. Repository Package:

    - Contains interfaces that perform database operations realted to an entity
    - These Interfaces extend JPARepository and uses Spring Data JPA to implement the crud operations
    - Package name for say book entity will be `com.dgv.book.repository`
    
4. Exception Package:

    - Contains custom exception classes which are thrown when exception occurs in the application
    - Package name for say authentication excpetion can be written as `com.dgv.exception.auth`

5. Configuration Package:
    
    - Contains configuration classes
    - define beans, properties and other settings, that configure the application's behavior.
    - Package for configuration class of say database can be written as `com.dgv.config.database`
6. Model Package:

    - Contains POJO classes annotated with @Entity
    - Package for an entity say book will be `com.dgv.book.model`
7. DTO Package:

    - Conatains Data Transfer Objects, used to abstract away the details of the models and transfer data between different application layers.
    - Package can be named as `com.dgv.book.dto`
8. Security Package:

    - Contains classes that provide security to the application
    - Includes classes such as Security Configurations, Authentication providers and filters.
    - package name can be based on type of security configurations for example jwt configuration class will have name `com.dgv.security.jwt`

# Database

## MySQL

- SQL qurey to view Triggers present in the database
```SQL
SELECT TRIGGER_NAME, ACTION_TIMING, EVENT_MANIPULATION, ACTION_STATEMENT
FROM INFORMATION_SCHEMA.TRIGGERS
WHERE TRIGGER_SCHEMA = 'your_database_name';
```

- SQL query to drop exisiting trigger
```SQL
DROP TRIGGER IF EXISTS trigger_name;
```

## Finding tables, procedures and triggers using specified fields

```sql
-- Query to find all table names from schemas containing given column_name
SELECT TABLE_NAME
FROM INFORMATION_SCHEMA.COLUMNS
WHERE COLUMN_NAME = 'column_name'
  AND TABLE_SCHEMA = 'db_name';

-- Query to find all procedure names from schemas containing given column_name
SELECT ROUTINE_NAME
FROM INFORMATION_SCHEMA.ROUTINES
WHERE ROUTINE_TYPE = 'PROCEDURE'
  AND ROUTINE_DEFINITION LIKE 'column_name'
  AND ROUTINE_SCHEMA = 'db_name';

-- Query to find all trigger names from schemas containing given column_name
SELECT TRIGGER_NAME
FROM INFORMATION_SCHEMA.TRIGGERS
WHERE ACTION_STATEMENT LIKE 'column_name'
  AND TRIGGER_SCHEMA = 'db_name';

-- query to find all columns and corresponding tables with given name
SELECT column_name, table_name
FROM information_schema.columns
WHERE table_schema = 'dgv_federal' and column_name like '%ciety%'
ORDER BY ordinal_position;

```


# Git

```bash
# creating a branch and switchin to it
git switch -c 'branch_name'

# checking status of repository
git status

# to add all files
git add .

# commiting the added files
git commit -m "message to be give"

# setting configs
git config --global user.email "email_id"
git config --global user.name "user_name"

# pushing the commited files
git push origin branch_name

# reverting the changes 
git rever hash_code_of_last_good_commit


```

- in gitlab
    
    - assignee = mentor
    - reviewer = mentor



# Cyber Security

## Phishing

  - hacking using email
  - tricking into clicking on virtual meets link
  - using teams/zoom to install malicious software

## Malware

    - Malicious software
      - How to identify?
        - Slow computer performance
        - Frequent popup adds
        - problem shutting down and getting up the system
      - How to avoid?
        - never click on link in pop up
        - only visit the sites that you trust
        - Login directly by going to the website
    
    - Websites and links
      - How to know if they are secure?
        - for example:
          - legitimate website: https://blog.knowbe4.com/ => https:// transfer protocol, blog => subdomain, knowbe4.com => domain
          - illegitimate website: https://blog-knowbe4.com/
      - Check for website domain using search engine, or if in doubt report back to the it team
    
    - UnSecure Wifi connections
      - public wifi's (coffee shop, airport)
      - Donot access confidential info over public wifi's
      - Use organization approved vpn to connect to ensure safe internet connection to shield from online threats
    
    - BEC => Business Email Compromise
      - Using email id of someone you know within the organization
      - How to identify?
        - Last minute changes/ urgent request
        - asking for tasks that isn't part of your responsibility
      - check for slight changes within the email
      - verify with them by speaking to them in person or using their phone number

## Pretexting

  - Scam tactic used by a hacker to create a realistic situation that makes you more likely to trust them and divulge information.




# SpringBoot

## Accessing two scehmams from same Spring boot application

- If there are two Databases say `dgv_federal` and `dgv_hdfc` and one has to access both these databases from the same spring boot application, a seperate config file must be created. Following code will show how to make it possible

1. application.properties file
```PROPERITES
# hdfc Configuration
hdfc.datasource.url=jdbc:mysql://172.33.1.8:3306/dgv_hdfc
hdfc.datasource.username=authuser1
hdfc.datasource.password=Pass@1234
hdfc.datasource.driver-class-name=com.mysql.cj.jdbc.Driver

# federal 2 Configuration
federal.datasource.url=jdbc:mysql://172.33.1.8:3306/dgv_federal
federal.datasource.username=authuser1
federal.datasource.password=Pass@1234
federal.datasource.driver-class-name=com.mysql.cj.jdbc.Driver

# hibernate configurations
spring.jpa.hibernate.ddl-auto=none
spring.jpa.properties.dialect=org.hibernate.dialect.MySQL8Dialect
spring.jpa.show-sql=true

```

2. Configuration files for various database and their JPA repositories

- HDFC database
```JAVA
package com.dgv.portal.config;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@EnableJpaRepositories(basePackages = "com.dgv.portal.hdfc.repository", entityManagerFactoryRef = "hdfcEntityManagerFactory", transactionManagerRef = "hdfcTransactionManager")
public class HdfcDatabaseConfig {

	@Autowired
	Environment environment;

	public class PersistenceUserConfiguration {
		@Autowired
		private Environment env;

		@Bean
		@Primary
		public LocalContainerEntityManagerFactoryBean hdfcEntityManagerFactory() {
			LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
			em.setDataSource(hdfcDataSource());
      /*
      * pacakge contiaining the models representing the tables present in the database
      */ 
			em.setPackagesToScan(new String[] { "com.dgv.portal.hdfc.model" });

			HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
			em.setJpaVendorAdapter(vendorAdapter);
			HashMap<String, Object> properties = new HashMap<>();
			properties.put("hibernate.hbm2ddl.auto", env.getProperty("spring.jpa.hibernate.ddl-auto"));
			properties.put("hibernate.dialect", env.getProperty("spring.jpa.properties.dialect"));
			em.setJpaPropertyMap(properties);

			return em;
		}

		@Primary
		@Bean
		public DataSource hdfcDataSource() {

			DriverManagerDataSource dataSource = new DriverManagerDataSource();
			dataSource.setDriverClassName(env.getProperty("hdfc.datasource.driver-class-name"));
			dataSource.setUrl(env.getProperty("hdfc.datasource.url"));
			dataSource.setUsername(env.getProperty("hdfc.datasource.username"));
			dataSource.setPassword(env.getProperty("hdfc.datasource.password"));
			log.info(dataSource.toString());
			return dataSource;
		}

		@Primary
		@Bean
		public PlatformTransactionManager hdfcTransactionManager() {

			JpaTransactionManager transactionManager = new JpaTransactionManager();
			transactionManager.setEntityManagerFactory(hdfcEntityManagerFactory().getObject());
			return transactionManager;
		}
	}
}
```
- Federal database
```JAVA
package com.dgv.portal.config;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@EnableJpaRepositories(basePackages = "com.dgv.portal.federal.repository", entityManagerFactoryRef = "federalEntityManager", transactionManagerRef = "federalTransactionManager")
public class FederalDatabaseConfig {

	@Autowired
	Environment environment;

	public class PersistenceUserConfiguration {
		@Autowired
		private Environment env;

		@Bean
		public LocalContainerEntityManagerFactoryBean federalEntityManager() {
			LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
			em.setDataSource(federalDataSource());
			em.setPackagesToScan(new String[] { "com.dgv.portal.federal.model" });

			HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
			em.setJpaVendorAdapter(vendorAdapter);
			HashMap<String, Object> properties = new HashMap<>();
			properties.put("hibernate.hbm2ddl.auto", env.getProperty("spring.jpa.hibernate.ddl-auto"));
			properties.put("hibernate.dialect", env.getProperty("spring.jpa.properties.dialect"));
			em.setJpaPropertyMap(properties);

			return em;
		}

		@Bean
		public DataSource federalDataSource() {

			DriverManagerDataSource dataSource = new DriverManagerDataSource();
			dataSource.setDriverClassName(env.getProperty("federal.datasource.driver-class-name"));
			dataSource.setUrl(env.getProperty("federal.datasource.url"));
			dataSource.setUsername(env.getProperty("federal.datasource.username"));
			dataSource.setPassword(env.getProperty("federal.datasource.password"));
			log.info(dataSource.toString());
			return dataSource;
		}

		@Bean
		public PlatformTransactionManager federalTransactionManager() {

			JpaTransactionManager transactionManager = new JpaTransactionManager();
			transactionManager.setEntityManagerFactory(federalEntityManager().getObject());
			return transactionManager;
		}
	}
}
```




## General Application properties file

```yaml
spring.datasource.url=jdbc:mysql://172.33.1.8:3306/training_database
spring.datasource.username=training_user
spring.datasource.password=Training@123
server.servlet.context-path=/bank


# Hibernate properties
spring.jpa.database-platform=org.hibernate.dialect.MySQLDialect
spring.jpa.hibernate.ddl-auto=update
spring.jpa.show-sql=true
spring.jpa.properties.hibernate.format_sql=true
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
```


## Swagger

- SpringFox dependency Maven
```XML
<dependency>
		   <groupId>io.springfox</groupId>
		   <artifactId>springfox-swagger2</artifactId>
		   <version>2.7.0</version>
</dependency>
```

Spring's documentation dependecy (Alternative to SpringFox and Swagger UI)
```XML
<dependency>
			<groupId>org.springdoc</groupId>
			<artifactId>springdoc-openapi-ui</artifactId>
			<version>1.6.4</version>
		</dependency>
```

- SpringFox Swagger ui Dependency maven
```XML
<dependency>
		   <groupId>io.springfox</groupId>
		   <artifactId>springfox-swagger-ui</artifactId>
		   <version>2.7.0</version>
</dependency>
```


### Errors and solutions

1. `documentationPluginsBootstrapper` null pointer exception

      CAUSE:  Spring Boot 2+ set as default the PathPathern-based matcher, while Spring Fox expects the Ant-based matcher. The problem with this solution is that you cannot use Spring Actuator, since it uses PathPattern based URL matching.

      SOLUTION: 
      Add the following line to `application.properties` file
```properties
spring.mvc.pathmatch.matching-strategy=ant-path-matcher
```


## Spring boot Acutators

- Maven Dependency
```XML
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>

```
- To expose all the endpoints of Acutators add the following in application.properties file
```properties
management.endpoints.web.exposure.include=*
```
- To exclude certain endpoints, include the following lines in your application.properties file
```properties
management.endpoints.web.exposure.exclude=jolokia,liquibase
```

# Terminal/cmd

- Start and stop process in terminal
```bash
# to get all the processes running
get-process

#to stop a running process by its name
stop-process -Name "process_name"
```

# Redmine

## Installation process

### For Ubuntu

**Step 1: Update packages and install dependencies**

Open your terminal and update all your packages using the following command:

```
sudo apt-get update && sudo apt-get upgrade
```

After upgrading all the packages, it’s essential to install the dependencies required for redmine. Run the following command:

```
sudo apt-get install -y apache2 mysql-server ruby-full build-essential libmysqlclient-dev libmagick++-dev libmagickwand-dev imagemagick libcurl4-openssl-dev nodejs
```

**Step 2: Install MySQL and create a database for Redmine**

Run the following command to install MySQL:

```
sudo apt-get install -y mysql-server
```

During the installation process, a root password will be prompted. Enter a secure password of your choice.

After the installation process is complete, log in to the mysql shell as root user:

```
sudo mysql -u root -p
```

Enter the root password when prompted.

Now create a new database and user for Redmine:

```
CREATE DATABASE redmine CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON redmine.* TO 'redmine'@'localhost';
FLUSH PRIVILEGES;
exit;
```

Replace `<password>` with a secure password of your choice.

**Step 3: Install Redmine**

Download the latest stable version of Redmine from the official website:

```
cd /opt/
wget https://www.redmine.org/releases/redmine-4.2.1.tar.gz
```

Extract the downloaded archive and rename the extracted folder:

```
tar xvzf redmine-4.2.1.tar.gz
mv redmine-4.2.1 redmine
```

Create a symbolic link for Redmine in your Apache configuration directory:

```
sudo ln -s /opt/redmine/public /var/www/html/redmine
```

**Step 4: Configure Redmine**

Copy the example configuration file for database:

```
cd /opt/redmine/config/
cp database.yml.example database.yml
```

Edit the configuration file using nano or any other command-line text editor:

```
nano database.yml
```

Add the following lines to the `production` section of the file:

```
production:
  adapter: mysql2
  database: redmine
  host: localhost
  username: redmine
  password: <password>
  encoding: utf8mb4
```

Replace `<password>` with the password you set in Step 2.

**Step 5: Install and configure bundler**

Install bundler to manage gem dependencies for Redmine:

```
gem install bundler
cd /opt/redmine/
bundle install --without development test
```

**Step 6: Start Redmine**

Now start Redmine using the following command:

```
RAILS_ENV=production rails server -b 0.0.0.0 -d
```

This will start Redmine on port 3000.

**Step 7: Access Redmine**

Open your web browser and access the following URL:

```
http://your_server_ip:3000
```

Congratulations, Redmine is now installed and ready to use with MySQL!


# Ubuntu

## Linux Shell commands

- Getting list of all the installed softwares
```bash
sudo apt list --installed
```

- Uninstalling a package/packages
```bash
# unistalling a specific package
sudo apt-get remove mysql-server

# uninstalling multiple packages of same domain
sudo apt-get remove mysql*
```
## Installing Mysql on ubuntu

1. Downloading and installing the package
```bash
sudo apt-get install mysql-server
```
2. login to the mysql server
```bash
sudo mysql -u root -p
# enter your sudo user password to login
```
3. Changing the password constraints
```SQL
-- while in the mysql server
show variables like 'validate_password%';
-- this will list a table containing all the constraints for the password.
-- To change the constraints use the following commands
set global validate_password.policy = 'LOW';
```

4. Set the root password
```SQL
alter user 'root'@'localhost' identified with mysql_native_password by 'root';
```

# Prompts

## Prompt for generating questions to learn a specific topic

```
Generate all the possible questions that I need to ask and get answered for learning XXX thoroughly.
```

# GraphQL



